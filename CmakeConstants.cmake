cmake_minimum_required(VERSION 2.6)

set(BUILD_FILES_DIR D:/Development/Libs CACHE STRING "Build files dir")
set(WIN32_EXE WIN32 CACHE STRING "Win 32 target")
set(DEBUG_FOLDER Debug CACHE STRING "Debug folder")
set(RELEASE_FOLDER Release CACHE STRING "Release folder")
set(DEBUG_PREFIX _d CACHE STRING "Debug prefix")