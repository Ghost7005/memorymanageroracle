@echo off
@set BUILDDIR_86=Build-x86
@set BUILDDIR_64=Build-x64
@set PROJECT_DIR=..
@set COMPILER_86="Visual Studio 14 2015"
@set COMPILER_64="Visual Studio 14 2015 Win64"
@set TOOL="v140"


rem by default use 4 cores config
IF "%CORES%"=="" set "CORES=4"
set coresArg=/m:%CORES%
set coresArg=%coresArg:"=%
echo "Cores count: %CORES%"

mkdir %BUILDDIR_86%
cd %BUILDDIR_86%
cmake -G %COMPILER_86% -T %TOOL% %PROJECT_DIR%
cmake -G %COMPILER_86% -T %TOOL% %PROJECT_DIR%
IF ERRORLEVEL 1 GOTO FAIL
cmake --build . --config RelWithDebInfo -- %coresArg%
IF ERRORLEVEL 1 GOTO FAIL

cd ..

rem x64 files build
rem rmdir /S /Q Build-x64
mkdir %BUILDDIR_64%
cd %BUILDDIR_64%
cmake -G %COMPILER_64% -T %TOOL% %PROJECT_DIR%
cmake -G %COMPILER_64% -T %TOOL% %PROJECT_DIR%
IF ERRORLEVEL 1 GOTO FAIL
cmake --build . --config RelWithDebInfo -- %coresArg%
IF ERRORLEVEL 1 GOTO FAIL

cd ..

echo "Build completed successfully"
GOTO OK

:FAIL
echo "Error occured."
cd ..

:OK
