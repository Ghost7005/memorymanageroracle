#include "catch.hpp"
#include <iostream>
#include "Allocators.h"
#include "BitmappedPoolAllocationStrategy.h"
#include "Chunk.h"
#include "FixedMemoryPool.h"
#include "MemoryObject.h"
#include <string.h>
#include <cstdint>

using namespace memory_managment;

const unsigned char mask = 0xFF;

bool Validate(void* memory, std::size_t size)
{
    auto buf = static_cast<std::uint8_t*>(memory);
    auto ch = (char*)::memchr(buf, mask, size);
    if (ch)
    {
        return true;
    }

    return false;
}

class Test
{
public:
    using Memory = SingletonFixedMemoryPool<StaticMemoryAllocator<2048>, BitmappedPoolAllocationStrategy>;

public:
    Test() : 
    answer(::mask)
    {
        std::cout << "Call ctor" << std::endl;
    };

    ~Test()
    {
        std::cout << "Call dtor" << std::endl;
    }

public:
    static void* operator new(std::size_t size)
    {
        auto mem = Memory::Instance().Allocate();
        return mem;
    }
    static void operator delete(void* p)
    {
        Memory::Instance().Free(p);
    }

private:
    unsigned char answer;
};

TEST_CASE("StoreTest")
{
    auto size = sizeof(Test);
    Test::Memory::Instance(size, 10);
    auto t = new Test();
    REQUIRE(Validate(t, size));
    delete t;

    auto allocator = StaticMemoryAllocator<4048>();
    FixedMemoryPool<decltype(allocator), BitmappedPoolAllocationStrategy> pool(1000, 2);
    auto mem = pool.Allocate();
    REQUIRE(mem != nullptr);
    pool.Free(mem);
    auto mem2 = pool.Allocate();   
    pool.Free(mem2);
    auto mem3 = pool.Allocate();
    pool.Free(mem3);
}