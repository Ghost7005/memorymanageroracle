#pragma once
#include "MemoryTraits.h"

namespace memory_managment
{
    template<std::size_t bufferSize>
    struct StaticMemoryAllocator
    {
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using block_size_t = typename MemoryTraits::block_size_t;
        using memory_t = typename MemoryTraits::memory_t;

        void Init()
        {
            curOffset = 0;
        }

        memory_pointer_t Allocate(block_size_t blockSize)
        {
            if (curOffset + blockSize < bufferSize)
            {
                curOffset += blockSize;
                return new (memory + curOffset) memory_t[bufferSize];
            }

            return nullptr;
        }

        void Free(memory_pointer_t memory, block_size_t blockSize)
        {
            std::memset(memory, 0, blockSize * sizeof(memory_t));
        }

        memory_t memory[bufferSize];
        block_size_t curOffset;
    };

    struct DynamicNewDeleteAllocator
    {
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using block_size_t = typename MemoryTraits::block_size_t;
        using memory_t = typename MemoryTraits::memory_t;
        
        void Init()
        {
        }

        memory_pointer_t Allocate(std::size_t size)
        {
            return new (std::nothrow) memory_t[size];
        }
        void Free(memory_pointer_t memory, block_size_t blockSize)
        {
            delete[] memory;
        }
    };

    struct DynamicMallocFreeAllocator
    {
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using block_size_t = typename MemoryTraits::block_size_t;
        using memory_t = typename MemoryTraits::memory_t;

        void Init()
        {

        }

        memory_pointer_t Allocate(std::size_t size)
        {
            return static_cast<memory_pointer_t>(std::malloc(size));
        }
        void Free(memory_pointer_t memory, block_size_t blockSize)
        {
            std::free(memory);
        }
    };
}

