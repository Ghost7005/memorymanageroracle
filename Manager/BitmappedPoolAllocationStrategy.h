#pragma once
#include "MemoryTraits.h"
#include "Allocators.h"
#include "Chunk.h"
#include <cstdint>
#include <type_traits>
#include <algorithm>
#include "MemoryTraits.h"

namespace memory_managment
{
    template<typename Allocator>
    class BitmappedPoolAllocationStrategy
    {
    private:
        using Chunks = std::vector<Chunk>;
        using ChuncksIterator = typename Chunks::iterator;
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using num_blocks_t = typename MemoryTraits::num_blocks_t;
        using blocks_size_t = typename MemoryTraits::block_size_t;

    public:
        BitmappedPoolAllocationStrategy(blocks_size_t _blocksSize = 4, num_blocks_t _numBlocks = 255)
         : blocksSize(_blocksSize),
         numBlocks(_numBlocks),
         allocChunk(nullptr),
         freeChunk(nullptr)
        {
            allocator.Init();
        }

        memory_pointer_t Allocate();
        void Free(memory_pointer_t memory);
        void FreeAllocation();
        
    private:
        bool IsBelongsToChunk(Chunk* chunk, memory_pointer_t memory)
        {
            return (memory >= chunk->data) && (memory <= chunk->data + blocksSize * numBlocks);
        }

        Chunk* AddChunk();
        ChuncksIterator FindChunk(memory_pointer_t memory);
        void EraseChunk(ChuncksIterator curChunkIter);

    private:
        blocks_size_t blocksSize;
        num_blocks_t numBlocks;
        Chunks chunks;
        Allocator allocator;
        ChuncksIterator freeBlockIter;
        Chunk* allocChunk;
        Chunk* freeChunk;
    };

    template<typename Allocator>
    typename BitmappedPoolAllocationStrategy<Allocator>::memory_pointer_t memory_managment::BitmappedPoolAllocationStrategy<Allocator>::Allocate()
    {
        if (allocChunk &&
            !allocChunk->IsFull())
        {
            return allocChunk->Allocate(blocksSize);
        }
        else
        {
            auto chunksIter = std::find_if(chunks.begin(), chunks.end(), [](auto chunk) {
                return chunk.blocksAvailable > 0;
            });

            if (chunksIter != chunks.end())
            {
                allocChunk = &*chunksIter;
                return allocChunk->Allocate(blocksSize);
            }
            else
            {
                if (AddChunk())
                {
                    return allocChunk->Allocate(blocksSize);
                }
            }
        }

        return nullptr;
    }

    template<typename Allocator>
    void memory_managment::BitmappedPoolAllocationStrategy<Allocator>::Free(memory_pointer_t memory)
    {
        if (!memory || chunks.empty())
            return;

        if (freeChunk && IsBelongsToChunk(freeChunk, memory))
        {
            freeChunk->Free(memory, blocksSize);
            EraseChunk(freeBlockIter);
        }
        else
        {
            auto chunkIter = FindChunk(memory);
            if (chunkIter != chunks.end())
            {
                chunkIter->Free(memory, blocksSize);
                EraseChunk(chunkIter);
            }
        }
    }

    template<typename Allocator>
    void memory_managment::BitmappedPoolAllocationStrategy<Allocator>::FreeAllocation()
    {
        for (auto& chunk : chunks)
        {
            allocator.Free(chunk.data, blocksSize);
        }
        chunks.clear();
    }

    template<typename Allocator>
    Chunk* memory_managment::BitmappedPoolAllocationStrategy<Allocator>::AddChunk()
    {
        auto buffer = allocator.Allocate(blocksSize * numBlocks);
        if (buffer)
        {
            auto newChunk = Chunk();
            newChunk.Init(buffer, blocksSize, numBlocks);
            chunks.reserve(chunks.size() + 1);
            chunks.emplace_back(newChunk);
            allocChunk = &chunks.back();
            freeChunk = &chunks.back();
            freeBlockIter = std::prev(chunks.end());
            return allocChunk;
        }

        return nullptr;
    }

    template<typename Allocator>
    void memory_managment::BitmappedPoolAllocationStrategy<Allocator>::EraseChunk(ChuncksIterator curChunkIter)
    {
        if (curChunkIter->IsEmpty(numBlocks) && chunks.size() > 1)
        {
            allocator.Free(curChunkIter->data, blocksSize);
            std::iter_swap(curChunkIter, std::prev(chunks.end()));
            chunks.pop_back();

            freeChunk = &chunks.back();
            allocChunk = &chunks.back();
            freeBlockIter = std::prev(chunks.end());
        }
    }

    template<typename Allocator>
    typename BitmappedPoolAllocationStrategy<Allocator>::ChuncksIterator memory_managment::BitmappedPoolAllocationStrategy<Allocator>::FindChunk(memory_pointer_t memory)
    {
        ChuncksIterator reverseIter = freeBlockIter;
        auto begin = chunks.begin();
        auto end = chunks.end();

        while (freeBlockIter != end || reverseIter != begin)
        {
            if (IsBelongsToChunk(&*freeBlockIter, memory))
            {
                return freeBlockIter;
            }
            else if (IsBelongsToChunk(&*reverseIter, memory))
            {
                return reverseIter;
            }
            if (freeBlockIter != end)
            {
                ++freeBlockIter;
            }
            if (reverseIter != begin)
            {
                --reverseIter;
            }
        }

        return chunks.end();
    }
}

