#pragma once
#include <assert.h>
#include "MemoryTraits.h"

namespace memory_managment
{
    struct Chunk
    {
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using blocks_size_t = typename MemoryTraits::block_size_t;
        using num_blocks_t = typename MemoryTraits::num_blocks_t;

        void Init(memory_pointer_t _data, blocks_size_t blockSize, num_blocks_t blocks)
        {
            data = _data;
            firstAvailableBlock = 0;
            blocksAvailable = blocks;
            memory_pointer_t p = data;
            if (data)
            {
                auto i = num_blocks_t();
                for (; i != blocks; p += blockSize)
                {
                    *p = ++i;
                }
            }
        }

        memory_pointer_t Allocate(blocks_size_t blockSize)
        {
            if (!blocksAvailable) return nullptr;

            auto nextBlock = data + (firstAvailableBlock * blockSize);
            --blocksAvailable;
            firstAvailableBlock = *nextBlock;

            return nextBlock;

        }
        void Free(memory_pointer_t p, blocks_size_t blocksSize)
        {
            assert(p >= data);
            auto dataToRelease = p;
            assert((p - data) % blocksSize == 0);
            *dataToRelease = firstAvailableBlock;
            firstAvailableBlock = static_cast<std::uint8_t>(
                (dataToRelease - data) / blocksSize);

            assert(firstAvailableBlock ==
                (dataToRelease - data) / blocksSize);
            ++blocksAvailable;
        }

        bool IsEmpty(num_blocks_t numBlocks)
        {
            return blocksAvailable == numBlocks;
        }

        bool IsFull()
        {
            return blocksAvailable == 0;
        }

        memory_pointer_t data;
        num_blocks_t firstAvailableBlock;
        num_blocks_t blocksAvailable;
    };
}


