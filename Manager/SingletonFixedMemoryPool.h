#pragma once
#include "Allocators.h"
#include "BitmappedPoolAllocationStrategy.h"

namespace memory_managment
{
    template<typename Allocator = StaticMemoryAllocator<2048>, template<typename Allocator> class PoolAllocationStrategy = BitmappedPoolAllocationStrategy>
    class SingletonFixedMemoryPool
    {
    public:
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;

    public:
        SingletonFixedMemoryPool(const SingletonFixedMemoryPool& other) = delete;
        SingletonFixedMemoryPool(SingletonFixedMemoryPool&& other) = delete;
        SingletonFixedMemoryPool& operator=(const SingletonFixedMemoryPool& other) = delete;
        SingletonFixedMemoryPool& operator=(SingletonFixedMemoryPool&& other) = delete;

    public:
        static SingletonFixedMemoryPool& Instance(std::size_t _blocksSize = 4, std::uint8_t _numBlocks = 255)
        {
            static SingletonFixedMemoryPool<Allocator, PoolAllocationStrategy> pool(_blocksSize, _numBlocks);

            return pool;
        }

        void* Allocate()
        {
            return poolAllocationStrategy.Allocate();
        }

        void Free(void* p)
        {
            poolAllocationStrategy.Free(static_cast<memory_pointer_t>(p));
        }

    protected:
        SingletonFixedMemoryPool(std::size_t _blocksSize = 4, std::uint8_t _numBlocks = 255)
        : poolAllocationStrategy(_blocksSize, _numBlocks)
        {

        }

    private:
        PoolAllocationStrategy<Allocator> poolAllocationStrategy;
    };
}

