#pragma once
#include "MemoryTraits.h"

namespace memory_managment
{
    template<typename Allocator = StaticMemoryAllocator<2048>, template<typename Allocator> class PoolAllocationStrategy = BitmappedPoolAllocationStrategy>
    class FixedMemoryPool
    {
    public:
        using memory_pointer_t = typename MemoryTraits::memory_pointer_t;
        using blocks_size_t = typename MemoryTraits::block_size_t;
        using num_blocks_t = typename MemoryTraits::num_blocks_t;

    public:
        FixedMemoryPool(blocks_size_t _blocksSize = 4, num_blocks_t _numBlocks = 255)
        : allocStrategy(_blocksSize, _numBlocks)
        {

        }
        ~FixedMemoryPool()
        {
            FreePoolMemory();
        }

    public:
        void* Allocate()
        {
            return allocStrategy.Allocate();
        }
        void Free(void* p)
        {
            allocStrategy.Free(static_cast<memory_pointer_t>(p));
        }

        void FreePoolMemory()
        {
            allocStrategy.FreeAllocation();
        }
    private:
        PoolAllocationStrategy<Allocator> allocStrategy;
    };
}

