#pragma once
#include <cstdint>
namespace memory_managment
{
    struct MemoryTraits
    {
        using memory_t = std::uint8_t;
        using memory_pointer_t = memory_t*;
        using block_size_t = std::size_t;
        using num_blocks_t = std::uint8_t;
    };
}

