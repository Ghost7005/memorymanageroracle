# README #

Overview: 
Implemented Memory Manager. The main goal was to provide fast memory allocation and deallocation for objects (mostly small objects).
The main design principle in lib was to use Pod(Plain Old Data) types and static polymorphism. (decrease memory overhead and increase allocation/deallocation 
speed) 
Brief overview of classes:
The bottom layer is a Chunk type. Each object of type Chunk contains and manages a chunk of memory consisting of an integral number of fixed size blocks.
Chunk contains logic that allows you to allocate and deallocate memory blocks. 
The next layer the BitmappedPoolAllocationStrategy class. A BitmappedPoolAllocationStrategy object uses Chunk as a building block. (allocates and deallocates chunks)
Allocator provided type of memory allocation (static, dynamic)
FixedMemoryPool,SingletonFixedMemoryPool container for Allocator and Strategy which provided main functionality.

Build and setup:
Following the steps below you should be able to properly build project

1) Clone repository
2) Install cmake 2.6 or greater
3) Start build using BuildScript

Implemented only Windows(MSVC) build.

